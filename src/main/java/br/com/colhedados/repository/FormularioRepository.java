package br.com.colhedados.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.colhedados.model.Formulario;

@Repository
public interface FormularioRepository extends JpaRepository<Formulario, Integer> {

}
