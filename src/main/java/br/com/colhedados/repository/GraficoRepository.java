package br.com.colhedados.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.colhedados.model.Formulario;
import br.com.colhedados.model.Grafico;
import br.com.colhedados.model.Questao;
import br.com.colhedados.model.Resposta;

@Repository
public interface GraficoRepository extends JpaRepository<Resposta, Integer> {

	@Query("SELECT R.resposta " + "FROM Resposta R " + "JOIN R.formulario F " + "JOIN R.questao Q "
			+ "WHERE F.id=:formId " + "AND Q.id=:questaoId " + "AND Q.tipoQuestao!=2 " + "GROUP BY Q.questao "
			+ "ORDER BY Q.questao")
	List<Resposta> countFormularioAndQuestao(@Param(value = "formId") Integer form_id,
			@Param(value = "questaoId") Integer questao_id);

}
