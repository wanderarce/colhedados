package br.com.colhedados.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.colhedados.model.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

	@Query("SELECT u "+
			"FROM Usuario u "+
			"WHERE u.username=:username "+ 
			"AND u.password=:password "
	)
	public Usuario findUsernameAndPassword(@Param("username") String username, @Param("password") String password);
}
