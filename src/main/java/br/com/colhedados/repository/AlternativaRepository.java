package br.com.colhedados.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.colhedados.model.Alternativa;

@Repository
public interface AlternativaRepository extends JpaRepository<Alternativa,Integer>{

	
}
