package br.com.colhedados.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.colhedados.model.TipoQuestao;

public interface TipoQuestaoRepository extends JpaRepository<TipoQuestao, Integer>{

}
