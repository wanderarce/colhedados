package br.com.colhedados.repository;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.colhedados.model.*;

@Repository
public interface RespostaRepository extends JpaRepository<Resposta, Integer> {

	
	
//	@Query(value="SELECT r.id,r.formulario_id,r.questao_id,r.resposta,r.version, COUNT(r.resposta) as series "+
//				"FROM Resposta r "+
//				"JOIN Formulario f "+
//				"JOIN Questao q  "+ 
//			"WHERE r.formulario_id=:formId "+ 
//				"AND r.questao_id=:questaoId "+
//				"AND q.tipo_questao_id!=2 "+
//				"GROUP BY q.questao, r.resposta "+
//			"ORDER BY q.questao,r.resposta",
//			nativeQuery=true
//			)
	@Query(value="SELECT r.id,r.formulario_id,r.questao_id,r.resposta,r.version, COUNT(r.resposta) as series "
						+ "FROM RESPOSTA r "
					+ " JOIN QUESTAO q ON q.ID=r.QUESTAO_ID " 
					+ " JOIN FORMULARIO f ON r.FORMULARIO_ID=f.ID "
					+ "WHERE q.TIPO_QUESTAO_ID!=2 " 
						+ "AND f.ID=:formId " 
						+ "AND q.ID=:questaoId " 
					+ "GROUP BY r.RESPOSTA "
					+ "ORDER BY r.RESPOSTA",
		nativeQuery=true)
	List<Resposta> countByFormularioAndQuestao(@Param(value="formId") Integer formId,
			@Param(value="questaoId") Integer questaoId);

}
