package br.com.colhedados.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.colhedados.model.Questao;

@Repository
public interface QuestaoRepository extends JpaRepository<Questao,Integer>{

}
