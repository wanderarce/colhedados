package br.com.colhedados;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.MimeMappings;

@SpringBootApplication
public class ColhedadosApplication {//implements EmbeddedServletContainerCustomizer{
	public static void main(String[] args) {
		SpringApplication.run(ColhedadosApplication.class, args);
	}

//	@Override
//	public void customize(ConfigurableEmbeddedServletContainer factory) {
//		MimeMappings mappings = new MimeMappings(MimeMappings.DEFAULT);
//        mappings.add("json", "application/json;charset=UTF-8;");
//        factory.setMimeMappings(mappings );
//   
//	}
}
