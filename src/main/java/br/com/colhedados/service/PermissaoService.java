package br.com.colhedados.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.colhedados.model.Permissao;
import br.com.colhedados.repository.PermissaoRepository;

@Service
public class PermissaoService {

	PermissaoRepository permissaoRepository;
	
	@Autowired
	public PermissaoService(PermissaoRepository permissaoRepository){
		this.permissaoRepository=permissaoRepository;
	}
	
	public Permissao create(Permissao permissao) {
		return permissaoRepository.save(permissao);
	}

	public Collection<Permissao> buscarPermissaos() {
		return permissaoRepository.findAll();
	}

	public Permissao buscarPermissaoPorId(Integer id) {
		return permissaoRepository.findOne(id);
	}

	public Permissao update(Permissao permissao) {
		return permissaoRepository.save(permissao);
	}

	public Permissao delete(Permissao id){
		if (id!=null) {
			permissaoRepository.delete(id);	
		}
		return id;
	}


	
}
