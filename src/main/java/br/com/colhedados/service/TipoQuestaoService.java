package br.com.colhedados.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.colhedados.model.TipoQuestao;
import br.com.colhedados.repository.TipoQuestaoRepository;

@Service
public class TipoQuestaoService {

	TipoQuestaoRepository tipoQuestaoRepository;

	@Autowired
	public TipoQuestaoService( TipoQuestaoRepository tipoQuestaoRepository){
		this.tipoQuestaoRepository=tipoQuestaoRepository;
	}
	
	public TipoQuestao create(TipoQuestao tipoQuestao) {
		return tipoQuestaoRepository.save(tipoQuestao);
	}

	public Collection<TipoQuestao> buscarTipoQuestoes() {
		return tipoQuestaoRepository.findAll();
	}

	public TipoQuestao buscarTipoQuestaoPorId(Integer id) {
		return tipoQuestaoRepository.findOne(id);
	}

	public TipoQuestao update(TipoQuestao tipoQuestao) {
		return tipoQuestaoRepository.save(tipoQuestao);
	}

	public TipoQuestao delete(TipoQuestao id){
		if (id!=null) {
			tipoQuestaoRepository.delete(id);	
		}
		return id;
	}

}
