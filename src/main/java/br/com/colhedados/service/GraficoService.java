package br.com.colhedados.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.colhedados.model.Resposta;
import br.com.colhedados.repository.RespostaRepository;

@Service
public class GraficoService {

	RespostaRepository respostaRepository;
	
	@Autowired
	public GraficoService(RespostaRepository respostaRepository) {
		this.respostaRepository = respostaRepository;
	}
		

	public List<Resposta> countByFormularioAndQuestao(Integer formulario,Integer questao) {
	
		List<Resposta> c=respostaRepository.countByFormularioAndQuestao(formulario,questao);

		System.out.println("service " + c.iterator().hasNext());
		for (Resposta resposta : c) {
			System.out.println("Service "+ resposta.getFormulario().getId() + ", "+ resposta.getResposta()
			+", "+ c.iterator().hasNext());
		}
		return c;
	}
	
	  
}
