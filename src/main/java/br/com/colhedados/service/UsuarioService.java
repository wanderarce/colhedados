package br.com.colhedados.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.colhedados.model.Usuario;
import br.com.colhedados.repository.UsuarioRepository;

@Service
public class UsuarioService {

	//@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	public UsuarioService(UsuarioRepository usuarioRepository){
		this.usuarioRepository=usuarioRepository;
	}
	public Usuario create(Usuario usuario){
		return usuarioRepository.save(usuario);
	}
	
	public Collection<Usuario> buscarUsuarios(){
		return usuarioRepository.findAll();
	}
	
	public Usuario buscarUsuarioPorId(Integer id){
		return usuarioRepository.findOne(id);
	}

	public void update(Usuario formulario){
		usuarioRepository.save(formulario);
	}
	
	public void delete(Integer id){
		usuarioRepository.delete(id);
		
	}
	public Usuario findLoginAndPassword(String username, String password) {
		return usuarioRepository.findUsernameAndPassword(username, password);
	}

}
