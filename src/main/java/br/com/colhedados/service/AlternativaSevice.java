package br.com.colhedados.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.colhedados.model.Alternativa;
import br.com.colhedados.repository.AlternativaRepository;

@Service
public class AlternativaSevice {

	AlternativaRepository alternativaRepository;
	
	@Autowired
	public AlternativaSevice(AlternativaRepository alternativaRepository){
		this.alternativaRepository=alternativaRepository;
	}
	
	public Alternativa create(Alternativa alternativa) {
		return alternativaRepository.save(alternativa);
	}

	public Collection<Alternativa> buscarAlternativas() {
		return alternativaRepository.findAll();
	}

	public Alternativa buscarAlternativaPorId(Integer id) {
		return alternativaRepository.findOne(id);
	}

	public Alternativa update(Alternativa alternativa) {
		return alternativaRepository.save(alternativa);
	}

	public Alternativa delete(Alternativa id){
		if (id!=null) {
			alternativaRepository.delete(id);	
		}
		return id;
	}

}
