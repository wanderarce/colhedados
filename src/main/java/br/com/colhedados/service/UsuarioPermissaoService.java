package br.com.colhedados.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.colhedados.model.UsuarioPermissao;
import br.com.colhedados.repository.UsuarioPermissaoRepository;

@Service
public class UsuarioPermissaoService {
	
	UsuarioPermissaoRepository usuarioPermissaoRepository;
	
	@Autowired
	public UsuarioPermissaoService(UsuarioPermissaoRepository usuarioPermissaoRepository){
		this.usuarioPermissaoRepository=usuarioPermissaoRepository;
	}
	public UsuarioPermissao create(UsuarioPermissao usuarioPermissao){
		return usuarioPermissaoRepository.save(usuarioPermissao);
	}
	
	public Collection<UsuarioPermissao> buscarUsuarioPermissoes(){
		return usuarioPermissaoRepository.findAll();
	}
	
	public void update(UsuarioPermissao usuarioPermissao){
		usuarioPermissaoRepository.save(usuarioPermissao);
	}
	
	
	public UsuarioPermissao buscarUsuarioPermissaoPorId(Integer id){
		return usuarioPermissaoRepository.findOne(id);
	}
	
	public void delete(UsuarioPermissao usuarioPermissaoExcluir){
		usuarioPermissaoRepository.delete(usuarioPermissaoExcluir);
		
	}

}
