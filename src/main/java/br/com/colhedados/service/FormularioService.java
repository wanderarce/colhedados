package br.com.colhedados.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.colhedados.model.Formulario;
import br.com.colhedados.repository.FormularioRepository;

@Service
public class FormularioService {

	FormularioRepository formularioRepository;
	
	@Autowired	
	public FormularioService(FormularioRepository formularioRepository){
		this.formularioRepository=formularioRepository;
	}
	
	public Formulario create(Formulario formulario) {
		return formularioRepository.save(formulario);
	}


	@Transactional
	public Collection<Formulario> buscarFormularios() {
		return formularioRepository.findAll();
	}

	public Formulario buscarFormularioPorId(Integer id) {
		return formularioRepository.findOne(id);
	}

	public Formulario update(Formulario formulario) {
		return formularioRepository.save(formulario);
	}

	public Formulario delete(Formulario id){
		if (id!=null) {
			formularioRepository.delete(id);	
		}
		return id;
	}
}
