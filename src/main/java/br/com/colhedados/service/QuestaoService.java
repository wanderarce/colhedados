package br.com.colhedados.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.colhedados.model.Questao;
import br.com.colhedados.repository.QuestaoRepository;

@Service
public class QuestaoService {


	@Autowired
	QuestaoRepository questaoRepository;
	
	public Questao create(Questao questao){
		return questaoRepository.save(questao);
	}
	
	public Collection<Questao> buscarQuestoes(){
		return questaoRepository.findAll();
	}
	
	public Questao buscarQuestaoPorId(Integer id){
		return questaoRepository.findOne(id);
	}

	public void update(Questao questao){
		questaoRepository.save(questao);
	}
	
	public void delete(Integer id){
		questaoRepository.delete(id);
	}

}
