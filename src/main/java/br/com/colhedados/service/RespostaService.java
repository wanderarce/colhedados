package br.com.colhedados.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.colhedados.model.Resposta;
import br.com.colhedados.repository.RespostaRepository;

@Service
public class RespostaService {

	RespostaRepository respostaRepository;
	
	@Autowired
	public RespostaService(RespostaRepository respostaRepository){
		this.respostaRepository=respostaRepository;
	}
	
	public Resposta create(Resposta Resposta){
		return respostaRepository.save(Resposta);
	}
	
	public Collection<Resposta> buscarRespostas(){
		return respostaRepository.findAll();
	}
	
	public Resposta buscarRespostaPorId(Integer id){
		return respostaRepository.findOne(id);
	}

	public void update(Resposta formulario){
		respostaRepository.save(formulario);
	}
	
	public Resposta delete(Resposta resposta){
		respostaRepository.delete(resposta);
		return resposta;	
	}

}
