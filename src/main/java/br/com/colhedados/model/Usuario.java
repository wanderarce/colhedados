package br.com.colhedados.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id",scope=Usuario.class)
@JsonAutoDetect
public class Usuario{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private Integer version;
	private boolean account_expired;
	private boolean account_locked;
	private boolean enabled;
	private String password;
	private boolean password_expired;
	private String username;
	
	@OneToMany(
			mappedBy="usuario",
			cascade=CascadeType.ALL,
			fetch = FetchType.LAZY
			)	
	//@JsonManagedReference("usuario-usuarioPermissao")
	private List<UsuarioPermissao> usuarioPermissoes= new ArrayList<UsuarioPermissao>();

	
	@OneToMany(
			mappedBy="usuario",
			cascade=CascadeType.ALL,
			fetch = FetchType.LAZY
			)	
	//@JsonManagedReference("formulario-usuario")
	private List<Formulario> formularios= new ArrayList<Formulario>();

	public Usuario() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public boolean isAccount_expired() {
		return account_expired;
	}

	public void setAccount_expired(boolean account_expired) {
		this.account_expired = account_expired;
	}

	public boolean isAccount_locked() {
		return account_locked;
	}

	public void setAccount_locked(boolean account_locked) {
		this.account_locked = account_locked;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isPassword_expired() {
		return password_expired;
	}

	public void setPassword_expired(boolean password_expired) {
		this.password_expired = password_expired;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public List<Formulario> getFormularios() {
		return formularios;
	}

	public void setFormularios(List<Formulario> formularios) {
		this.formularios = formularios;
	}

	public List<UsuarioPermissao> getUsuarioPermissoes() {
		return usuarioPermissoes;
	}
	
	public void setUsuarioPermissoes(List<UsuarioPermissao> usuarioPermissoes) {
		this.usuarioPermissoes = usuarioPermissoes;
	}
	
}
