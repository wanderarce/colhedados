package br.com.colhedados.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id",scope=Resposta.class)
public class Resposta{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private Integer version;
	
//	@Column(columnDefinition = "DATETIME")
//	private DateTimeFormat dateCreated;
//	@Column(columnDefinition = "DATETIME")
//	private DateTimeFormat lastUpdated;
	private String resposta;

	@ManyToOne
	@JoinColumn(name = "formulario_id")
	//@JsonBackReference("questao-resposta")
	private Formulario formulario;

	
	@ManyToOne
	@JoinColumn(name = "questao_id")
	//@JsonBackReference("questao-resposta")
	private Questao questao;
	
//	@OneToMany(mappedBy = "resposta",
//			cascade = CascadeType.ALL,
//			fetch = FetchType.EAGER
//			)
//	//@JsonManagedReference("resposta-formulario")
//	private List<Grafico> graficos = new ArrayList<Grafico>();

	public Resposta() {	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getResposta() {
		return resposta;
	}

	public void setResposta(String resposta) {
		this.resposta = resposta;
	}

	public Questao getQuestao() {
		return questao;
	}

	public void setQuestao(Questao questao) {
		this.questao = questao;
	}

	public Formulario getFormulario() {
		return formulario;
	}

	public void setFormulario(Formulario formulario) {
		this.formulario = formulario;
	}
	
//	public List<Grafico> getGraficos() {
//		return graficos;
//	}
//
//	public void setGraficos(List<Grafico> graficos) {
//		this.graficos = graficos;
//	}

}
