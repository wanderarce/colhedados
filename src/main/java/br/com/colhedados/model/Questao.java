package br.com.colhedados.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id",scope=Questao.class)
public class Questao {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer id;
	public String questao;
	public boolean ativo;
	
	@ManyToOne
	@JoinColumn(name = "formulario_id")
	//@JsonBackReference("formulario-questao")
	public Formulario formulario;

	@OneToMany(mappedBy="questao",
			cascade=CascadeType.ALL,
			fetch = FetchType.EAGER
			)	
	//@JsonManagedReference("alternativa-questao")
	private List<Alternativa> alternativas= new ArrayList<Alternativa>();
	
	@OneToMany(mappedBy="questao",
			cascade=CascadeType.ALL,
			fetch = FetchType.EAGER
			)	
	//@JsonManagedReference("resposta-questao")
	private List<Resposta> respostas= new ArrayList<Resposta>();

//	@OneToMany(mappedBy = "questao",
//			cascade = CascadeType.ALL,
//			fetch = FetchType.EAGER
//			)
//	//@JsonManagedReference("grafico-questao")
//	private List<Grafico> graficos = new ArrayList<Grafico>();

	@ManyToOne
	@JoinColumn(name = "tipo_questao_id")
	//@JsonBackReference("questao-tipoQuestao")
	private TipoQuestao tipoQuestao;
	
	
	public Questao() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getQuestao() {
		return questao;
	}

	public void setQuestao(String questao) {
		this.questao = questao;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Formulario getFormulario() {
		return formulario;
	}

	public void setFormulario(Formulario formulario) {
		this.formulario = formulario;
	}

	public List<Alternativa> getAlternativas() {
		return alternativas;
	}

	public void setAlternativas(List<Alternativa> alternativas) {
		this.alternativas = alternativas;
	}

	public List<Resposta> getRespostas() {
		return respostas;
	}

	public void setRespostas(List<Resposta> respostas) {
		this.respostas = respostas;
	}

//	public List<Grafico> getGraficos() {
//		return graficos;
//	}
//
//	public void setGraficos(List<Grafico> graficos) {
//		this.graficos = graficos;
//	}

	public TipoQuestao getTipoQuestao() {
		return tipoQuestao;
	}

	public void setTipoQuestao(TipoQuestao tipoQuestao) {
		this.tipoQuestao = tipoQuestao;
	}
	
}
