package br.com.colhedados.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id",scope=Alternativa.class)
public class Alternativa{
	@Id
	@GeneratedValue
	private Integer id;
	private Integer version;
	private boolean ativo;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name ="proxima_questao_id")
	//@JsonBackReference("questao")
	private Questao proximaQuestao;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name ="questao_id")
	//@JsonBackReference("questao-alternativa")
	private Questao questao;
	
	private String alternativa;
	
	public Alternativa() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Questao getProximaQuestao() {
		return proximaQuestao;
	}

	public void setProximaQuestao(Questao proximaQuestao) {
		this.proximaQuestao = proximaQuestao;
	}

	public Questao getQuestao() {
		return questao;
	}

	public void setQuestao(Questao questao) {
		this.questao = questao;
	}

	public String getAlternativa() {
		return alternativa;
	}

	public void setAlternativa(String alternativa) {
		this.alternativa = alternativa;
	}
	
}
