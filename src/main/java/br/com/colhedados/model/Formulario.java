package br.com.colhedados.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id",scope=Formulario.class)
public class Formulario {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private Integer version;
	private boolean publicado;
	private String formulario;
	private String nota;
	private boolean ativo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id")
	@JsonBackReference("usuario-formulario")
	private Usuario usuario;

	@OneToMany(mappedBy = "formulario",
			cascade = CascadeType.ALL,
			fetch = FetchType.EAGER
			)
	//@JsonManagedReference("questao-formulario")
	private List<Questao> questoes = new ArrayList<Questao>();
	
	@OneToMany(mappedBy = "formulario",
			cascade = CascadeType.ALL,
			fetch = FetchType.EAGER
			)
	//@JsonManagedReference("resposta-formulario")
	private List<Resposta> respostas= new ArrayList<Resposta>();
	
//	@OneToMany(mappedBy = "formulario",
//			cascade = CascadeType.ALL,
//			fetch = FetchType.EAGER
//			)
//	//@JsonManagedReference("grafico-formulario")
//	private List<Grafico> graficos = new ArrayList<Grafico>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVersion() {
		return version;
	}

	public boolean isPublicado() {
		return publicado;
	}

	public void setPublicado(boolean publicado) {
		this.publicado = publicado;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getFormulario() {
		return formulario;
	}

	public void setFormulario(String formulario) {
		this.formulario = formulario;
	}

	public String getNota() {
		return nota;
	}

	public void setNota(String nota) {
		this.nota = nota;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Questao> getQuestoes() {
		return questoes;
	}

	public void setQuestoes(List<Questao> questoes) {
		this.questoes = questoes;
	}

	public List<Resposta> getRespostas() {
		return respostas;
	}

	public void setRespostas(List<Resposta> respostas) {
		this.respostas = respostas;
	}
	
//	public List<Grafico> getGraficos() {
//		return graficos;
//	}
//
//	public void setGraficos(List<Grafico> graficos) {
//		this.graficos = graficos;
//	}
		
}
