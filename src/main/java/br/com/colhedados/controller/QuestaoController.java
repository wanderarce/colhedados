package br.com.colhedados.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.colhedados.model.Questao;
import br.com.colhedados.service.QuestaoService;

@RestController
@RequestMapping(value="/questoes")
public class QuestaoController {

	@Autowired
	QuestaoService questaoService;
	
	@RequestMapping(value="/create",method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Questao> save(@RequestBody Questao questao){
		
		return new ResponseEntity<Questao>(questao,HttpStatus.CREATED);
	}
	

	@RequestMapping(value="/",method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Collection<Questao>> buscarTodasQuestoess(){
		Collection<Questao> questoes= questaoService.buscarQuestoes();
		return new ResponseEntity<>(questoes, HttpStatus.OK);
	}
	
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Questao> buscarQuestaoPorId(@PathVariable Integer id){
		Questao questao=questaoService.buscarQuestaoPorId(id);
		return new ResponseEntity<>(questao, HttpStatus.OK);
		
	}

	@RequestMapping(value="/update/{id}",method=RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Questao> edit(@RequestBody Questao questao){
		return new ResponseEntity<Questao>(questao,HttpStatus.OK);
	}

	@RequestMapping(value="/delete/{id}",method=RequestMethod.DELETE)
	public ResponseEntity<Questao> deletarQuestao(@PathVariable Integer id){
		questaoService.delete(id);
		return new ResponseEntity<Questao>(HttpStatus.OK);
	}

}
