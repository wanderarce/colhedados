package br.com.colhedados.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.colhedados.model.TipoQuestao;
import br.com.colhedados.service.TipoQuestaoService;

@RestController
@RequestMapping(value="/tipoQuestao")
public class TipoQuestaoController {
	
	@Autowired
	TipoQuestaoService tipoQuestaoService;
	
	@RequestMapping(value="/create",method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TipoQuestao> save(@RequestBody TipoQuestao tipoQuestao){
		TipoQuestao tipoQuestaoSalvar=tipoQuestaoService.create(tipoQuestao);
		return new ResponseEntity <TipoQuestao>(tipoQuestaoSalvar,HttpStatus.CREATED);
	}
	

	@RequestMapping(value="/",method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity <Collection<TipoQuestao>> buscarTodosTiposQuestoes(){
		Collection<TipoQuestao> tipoQuestoes = tipoQuestaoService.buscarTipoQuestoes();
		return new ResponseEntity<>(tipoQuestoes,HttpStatus.OK);
	}
	
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<TipoQuestao> buscarTipoQuestaoPorId(@PathVariable Integer id){
		TipoQuestao tipoQuestao=tipoQuestaoService.buscarTipoQuestaoPorId(id);
		return new ResponseEntity<>(tipoQuestao, HttpStatus.OK);
		
	}

	@RequestMapping(value="/update/{id}",method=RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<TipoQuestao> edit(@RequestBody TipoQuestao tipoQuestao){
		return new ResponseEntity<TipoQuestao>(tipoQuestao,HttpStatus.OK);
	}

	@RequestMapping(value="/delete/{id}",method=RequestMethod.DELETE)
	public ResponseEntity<TipoQuestao> deletarFormulario(@PathVariable TipoQuestao id){
		TipoQuestao tipoQuestaoExcluir= tipoQuestaoService.delete(id);
		if(tipoQuestaoExcluir==null){
			return new ResponseEntity<TipoQuestao>(HttpStatus.NOT_FOUND);
		}
		tipoQuestaoService.delete(id);
		return new ResponseEntity<TipoQuestao>(HttpStatus.OK);
	}
}
