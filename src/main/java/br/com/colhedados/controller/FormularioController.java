package br.com.colhedados.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.colhedados.model.Formulario;
import br.com.colhedados.service.FormularioService;

@RestController
@RequestMapping(value="/formularios")
public class FormularioController {

	@Autowired
	FormularioService formularioService;
	
	@RequestMapping(value="/create",method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Formulario> save(@RequestBody Formulario formulario){
		Formulario formularioSalvar=formularioService.create(formulario);
		return new ResponseEntity <Formulario>(formularioSalvar,HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/",method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity <Collection<Formulario>> buscarTodosFromularios(){
		Collection<Formulario> formularios= formularioService.buscarFormularios();
		return new ResponseEntity<>(formularios,HttpStatus.OK);
	}
	
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Formulario> buscarFormularioPorId(@PathVariable Integer id){
		Formulario formulario=formularioService.buscarFormularioPorId(id);
		return new ResponseEntity<>(formulario, HttpStatus.OK);
		
	}

	@RequestMapping(value="/update/{id}",method=RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Formulario> edit(@RequestBody Formulario formulario){
		return new ResponseEntity<Formulario>(formulario,HttpStatus.OK);
	}

	@RequestMapping(value="/delete/{id}",method=RequestMethod.DELETE)
	public ResponseEntity<Formulario> deletarFormulario(@PathVariable Formulario id){
		Formulario formularioExcluir= formularioService.delete(id);
		if(formularioExcluir==null){
			return new ResponseEntity<Formulario>(HttpStatus.NOT_FOUND);
		}
		formularioService.delete(id);
		return new ResponseEntity<Formulario>(HttpStatus.OK);
	}
}
