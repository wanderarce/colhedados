package br.com.colhedados.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.colhedados.model.Resposta;
import br.com.colhedados.service.RespostaService;

@RestController
@RequestMapping(value="/respostas")
public class RespostaController {
	
	@Autowired
	RespostaService respostaService;
	
	@RequestMapping(value="/create",method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> save(@RequestBody Resposta resposta){
		//resposta.setDateCreated(new DateTime());
		System.out.println(resposta);
		return new ResponseEntity <Resposta>(resposta,HttpStatus.CREATED);
	}
	

	@RequestMapping(value="/",method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity <Collection<Resposta>> buscarTodosFromularios(){
		Collection<Resposta> respostas= respostaService.buscarRespostas();
		return new ResponseEntity<>(respostas,HttpStatus.OK);
	}
	
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Resposta> buscarRespostaPorId(@PathVariable Integer id){
		Resposta resposta=respostaService.buscarRespostaPorId(id);
		return new ResponseEntity<>(resposta, HttpStatus.OK);
		
	}

	@RequestMapping(value="/update/{id}",method=RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Resposta> edit(@RequestBody Resposta resposta){
		//resposta.setLastUpdated(new DateTime());
		return new ResponseEntity<Resposta>(resposta,HttpStatus.OK);
	}

	@RequestMapping(value="/delete/{id}",method=RequestMethod.DELETE)
	public ResponseEntity<Resposta> deletarResposta(@PathVariable Resposta id){
		Resposta respostaExcluir= respostaService.delete(id);
		if(respostaExcluir==null){
			return new ResponseEntity<Resposta>(HttpStatus.NOT_FOUND);
		}
		respostaService.delete(id);
		return new ResponseEntity<Resposta>(HttpStatus.OK);
	}

}
