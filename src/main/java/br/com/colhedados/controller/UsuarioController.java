package br.com.colhedados.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.colhedados.model.Usuario;
import br.com.colhedados.service.UsuarioService;

@RestController
@ComponentScan
@RequestMapping(value="/usuarios")
public class UsuarioController {

	@Autowired
	UsuarioService usuarioService;
	
	@RequestMapping(value="/create",method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Usuario> save(@RequestBody Usuario usuario){
		
		return new ResponseEntity<Usuario>(usuario,HttpStatus.CREATED);
	}
	

	@RequestMapping(value="/",method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Collection<Usuario>> buscarTodosFromularios(){
		Collection<Usuario> usuarios= usuarioService.buscarUsuarios();
		return new ResponseEntity<>(usuarios, HttpStatus.OK);
	}
	
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Usuario> buscarUsuarioPorId(@PathVariable Integer id){
		Usuario usuario=usuarioService.buscarUsuarioPorId(id);
		return new ResponseEntity<>(usuario, HttpStatus.OK);
		
	}

	@RequestMapping(value="/update/{id}",method=RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Usuario> edit(@RequestBody Usuario Usuario){
		return new ResponseEntity<Usuario>(Usuario,HttpStatus.OK);
	}

	
	@RequestMapping(value="/delete/{id}",method=RequestMethod.DELETE)
	public ResponseEntity<Usuario> deletarUsuario(@PathVariable Integer id){
		Usuario usuarioExcluir=usuarioService.buscarUsuarioPorId(id);
		if (usuarioExcluir==null) {
			return new ResponseEntity<Usuario>(HttpStatus.NOT_FOUND);
		}
		usuarioService.delete(id);
		return new ResponseEntity<Usuario>(HttpStatus.OK);
	}

}
