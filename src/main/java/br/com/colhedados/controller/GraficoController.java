package br.com.colhedados.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.colhedados.model.Resposta;
import br.com.colhedados.service.GraficoService;

@RestController
@RequestMapping(value = "/grafico")
public class GraficoController {
	@Autowired
	GraficoService graficoService;

	@RequestMapping(value = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Resposta>> findCountByFormularioIdAndQuestaoId(@RequestBody Resposta resposta) {
		List<Resposta> series =null;
		try {

			System.out.println("Controller: " + resposta);

			series= graficoService
					.countByFormularioAndQuestao(resposta.getFormulario().getId(), resposta.getQuestao().getId());
			List<Resposta> dados=new ArrayList<>();
			for (Resposta resposta2 : series) {
				System.out.printf(
						 "Formulario: " + resposta2.getFormulario().getId(),
						//"Questao: " + resposta2.getQuestao().getId()
					    "s:" + series.get(5)
						);
			}
			System.out.print("Aqui: ");
			if (series == null || series.isEmpty()) {
				System.out.println("Vazio");
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			e.getMessage();
		}
		return new ResponseEntity<>(series, HttpStatus.OK);

	}

}
