package br.com.colhedados.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.colhedados.model.Usuario;
import br.com.colhedados.service.UsuarioService;

@RestController
@RequestMapping(value="/login")
public class LoginController {
	
	@Autowired
	UsuarioService usuarioService;
	
	@RequestMapping(value="",method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE,produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Usuario> login(@RequestBody Usuario usuario){
		System.out.println("Login: " + usuario.getUsername() + " Senha: " + usuario.getPassword());
		
		Usuario usuarioAuth=usuarioService.findLoginAndPassword(usuario.getUsername(),usuario.getPassword());
		
		System.out.println(usuarioAuth);
		if(usuarioAuth==null){
			return new ResponseEntity<Usuario>(HttpStatus.UNAUTHORIZED);
		}
		return new ResponseEntity<>(usuarioAuth,HttpStatus.OK);
	}
	
	
	
}
