package br.com.colhedados.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.colhedados.model.Alternativa;
import br.com.colhedados.service.AlternativaSevice;

@RestController
@Transactional
@RequestMapping(value="/alternativas")
public class AlternativaController {

	@Autowired
	AlternativaSevice alternativaSevice;
	
	@RequestMapping(value="/create",method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Alternativa> save(@RequestBody Alternativa alternativa){
		return new ResponseEntity<>(alternativa,HttpStatus.CREATED);
	}
	

	@RequestMapping(value="/",method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity <Collection<Alternativa>> buscarTodasAlternativas(){
		Collection<Alternativa> alternativas= alternativaSevice.buscarAlternativas();
		return new ResponseEntity<>(alternativas,HttpStatus.OK);
	}
	
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Alternativa> buscarAlternativaPorId(@PathVariable Integer id){
		Alternativa alternativa=alternativaSevice.buscarAlternativaPorId(id);
		return new ResponseEntity<>(alternativa, HttpStatus.OK);
		
	}

	@RequestMapping(value="/update/{id}",method=RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Alternativa> edit(@RequestBody Alternativa alternativa){
		return new ResponseEntity<Alternativa>(alternativa,HttpStatus.OK);
	}

	@RequestMapping(value="/delete/{id}",method=RequestMethod.DELETE)
	public ResponseEntity<Alternativa> deletarAlternativa(@PathVariable Alternativa id){
		Alternativa alternativaExcluir= alternativaSevice.delete(id);
		if(alternativaExcluir==null){
			return new ResponseEntity<Alternativa>(HttpStatus.NOT_FOUND);
		}
		alternativaSevice.delete(id);
		return new ResponseEntity<Alternativa>(HttpStatus.OK);
	}

}
