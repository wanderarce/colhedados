package br.com.colhedados.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.colhedados.model.Usuario;
import br.com.colhedados.model.UsuarioPermissao;
import br.com.colhedados.service.UsuarioPermissaoService;

@RestController
@RequestMapping(value="/usuarioPermissoes")
public class UsuarioPermissaoController {

	@Autowired
	UsuarioPermissaoService usuarioPermissaoService;
	
	@RequestMapping(value="/create",method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<UsuarioPermissao> save(@RequestBody UsuarioPermissao usuarioPermissao){
		
		return new ResponseEntity<UsuarioPermissao>(usuarioPermissao,HttpStatus.CREATED);
	}
	

	@RequestMapping(value="/",method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Collection<UsuarioPermissao>> buscarTodosUsuariosPermissoes(){
		Collection<UsuarioPermissao> usuarioPermissoes= usuarioPermissaoService.buscarUsuarioPermissoes();
		return new ResponseEntity<>(usuarioPermissoes, HttpStatus.OK);
	}
	
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<UsuarioPermissao> buscarUsuarioPorId(@PathVariable Integer id){
		UsuarioPermissao usuarioPermissao=usuarioPermissaoService.buscarUsuarioPermissaoPorId(id);
		return new ResponseEntity<>(usuarioPermissao, HttpStatus.OK);
		
	}

	@RequestMapping(value="/update/{id}",method=RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<UsuarioPermissao> edit(@RequestBody UsuarioPermissao usuarioPermissao){
		return new ResponseEntity<UsuarioPermissao>(usuarioPermissao,HttpStatus.OK);
	}

	
	@RequestMapping(value="/delete/{id}",method=RequestMethod.DELETE)
	public ResponseEntity<UsuarioPermissao> deletarUsuario(@PathVariable Integer id){
		UsuarioPermissao usuarioPermissaoExcluir=usuarioPermissaoService.buscarUsuarioPermissaoPorId(id);
		if (usuarioPermissaoExcluir==null) {
			return new ResponseEntity<UsuarioPermissao>(HttpStatus.NOT_FOUND);
		}
		usuarioPermissaoService.delete(usuarioPermissaoExcluir);
		return new ResponseEntity<UsuarioPermissao>(HttpStatus.OK);
	}
	
	
}
