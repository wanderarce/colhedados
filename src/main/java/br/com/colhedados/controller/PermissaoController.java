package br.com.colhedados.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.colhedados.model.Permissao;
import br.com.colhedados.service.PermissaoService;

@RestController
@RequestMapping(value="/permissoes")
public class PermissaoController {

	@Autowired
	PermissaoService permissaoService;
	
	@RequestMapping(value="/create",method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Permissao> save(@RequestBody Permissao permissao){
		return new ResponseEntity<>(permissao,HttpStatus.CREATED);
	}
	

	@RequestMapping(value="/",method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity <Collection<Permissao>> buscarTodasPermissaos(){
		Collection<Permissao> permissaos= permissaoService.buscarPermissaos();
		return new ResponseEntity<>(permissaos,HttpStatus.OK);
	}
	
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Permissao> buscarPermissaoPorId(@PathVariable Integer id){
		Permissao permissao=permissaoService.buscarPermissaoPorId(id);
		return new ResponseEntity<>(permissao, HttpStatus.OK);
		
	}

	@RequestMapping(value="/update/{id}",method=RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Permissao> edit(@RequestBody Permissao permissao){
		return new ResponseEntity<Permissao>(permissao,HttpStatus.OK);
	}

	@RequestMapping(value="/delete/{id}",method=RequestMethod.DELETE)
	public ResponseEntity<Permissao> deletarPermissao(@PathVariable Permissao id){
		Permissao permissaoExcluir= permissaoService.delete(id);
		if(permissaoExcluir==null){
			return new ResponseEntity<Permissao>(HttpStatus.NOT_FOUND);
		}
		permissaoService.delete(id);
		return new ResponseEntity<Permissao>(HttpStatus.OK);
	}


}
